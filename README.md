# Elsevier test assignment

## Installation
```bash
> npm install
```
or
```bash
> yarn
```

Rename `.evn.dist` to `.env` and check all variables.


## Development server
```bash
> npm run start
```
or
```bash
> yarn start
```

## Production build
```bash
> npm run build
```
or
```bash
> yarn build
```