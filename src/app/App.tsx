import * as React from 'react'
import { Provider } from 'react-redux'
import { Store } from 'redux'
import { Header } from '~/components'
import Portfolio from '~/portfolio/Portfolio'
import { ApplicationState } from '~/store'

const APP_TITLE = 'GitHub Portfolio'

export interface AppProps {
  store: Store<ApplicationState>
}

const App: React.SFC<AppProps> = ({ store }) => (
  <Provider store={store}>
    <>
      <Header title={APP_TITLE} />
      <Portfolio />
    </>
  </Provider>
)

export default App
