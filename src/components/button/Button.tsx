import React, { HTMLProps, SFC } from 'react'

import styles from './Button.scss'

export interface ButtonProps extends HTMLProps<HTMLButtonElement> {
  children: string
}

const Button: SFC<ButtonProps> = ({ children, ...rest }) => (
  <button {...rest} className={styles.button}>
    {children}
  </button>
)

export default Button
