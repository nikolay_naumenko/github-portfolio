import React, { SFC } from 'react'

import styles from './Header.scss'

export interface HeaderProps {
  title: string
}

const Header: SFC<HeaderProps> = ({ title }) => (
  <div className={styles.header}>
    <h1 className={styles.headerHeading}>{title}</h1>
  </div>
)

export default Header
