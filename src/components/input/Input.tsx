import classNames from 'classnames'
import React, { HTMLProps, SFC } from 'react'

import styles from './input.scss'

export interface ButtonProps extends HTMLProps<HTMLInputElement> {
  label?: string
  isError: boolean
}

const Input: SFC<ButtonProps> = ({ label, isError, ...rest }) => (
  <>
    {label && <label className={styles.label}>{label}</label>}
    <input {...rest} className={classNames(styles.input, isError && styles.error)} />
  </>
)

export default Input
