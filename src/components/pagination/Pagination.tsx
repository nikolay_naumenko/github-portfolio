import React, { SFC } from 'react'
import { PageData, PageKeys, Pagination } from '~/portfolio/portfolio.types'
import PaginationLink from './PaginationLink'

import styles from './Pagination.scss'

export interface PaginationProps {
  pagination: Pagination
  onClick(e: React.MouseEvent, page: PageData)
}

const pagesOrder: PageKeys[] = ['first', 'prev', 'next', 'last']

const Pagination: SFC<PaginationProps> = ({ pagination, onClick }) => (
  <div className={styles.pagination}>
    {pagesOrder.map(
      (key: string) =>
        pagination[key] && (
          <PaginationLink page={pagination[key]} onClick={onClick} key={key}>
            {key}
          </PaginationLink>
        )
    )}
  </div>
)

export default Pagination
