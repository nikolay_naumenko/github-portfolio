import React, { SFC } from 'react'
import { PageData } from '~/portfolio/portfolio.types'

import styles from './PaginationLink.scss'

export interface PaginationLinkProps {
  children: string
  page: PageData
  onClick(e: React.MouseEvent, page: PageData)
}

const PaginationLink: SFC<PaginationLinkProps> = ({ children, page, onClick, ...rest }) => (
  <a {...rest} onClick={e => onClick(e, page)} href="#" className={styles.paginationLink}>
    {children}
  </a>
)

export default PaginationLink
