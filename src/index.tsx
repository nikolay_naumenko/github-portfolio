import React from 'react'
import ReactDOM from 'react-dom'
import { ApplicationState } from '~/store'
import App from './app/App'
import configureStore from './configureStore'

const initialState: ApplicationState = {
  repositories: {
    initial: true,
    loading: false,
    data: [],
    requestUrl: '',
    pagination: {},
  },
}

const store = configureStore(initialState)

ReactDOM.render(<App store={store} />, document.getElementById('app'))
