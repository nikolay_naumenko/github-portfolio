import React, { SFC } from 'react'
import { Contributor } from '~/portfolio/portfolio.types'

export interface ContributorListProps {
  contributors: Contributor[]
}

const ContributorsList: SFC<ContributorListProps> = ({ contributors }) => (
  <ul>
    {contributors && contributors.length === 0 && <span>No contributors</span>}
    {contributors &&
      contributors.map(contributor => (
        <li key={contributor.id}>
          {contributor.login} ({contributor.contributions} commit
          {contributor.contributions > 1 ? 's' : ''})
        </li>
      ))}
  </ul>
)

export default ContributorsList
