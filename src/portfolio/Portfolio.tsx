import React, { SFC } from 'react'
import RepositoryList from '~/portfolio/RepositoryList'
import SearchBar from './SearchBar'

export interface IPortfolioProps {}

const Portfolio: SFC<IPortfolioProps> = () => (
  <>
    <SearchBar />
    <RepositoryList />
  </>
)

export default Portfolio
