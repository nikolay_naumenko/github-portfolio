import React, { SFC } from 'react'
import { ContributorsList } from '~/portfolio'
import { Repository } from '~/portfolio/portfolio.types'

import styles from '~/portfolio/RepositoryItem.scss'

export interface RepositoryItemProps {
  repository: Repository
}

const RepositoryItem: SFC<RepositoryItemProps> = ({ repository }) => (
  <div key={repository.id} className={styles.repository}>
    <span className={styles.name}>{repository.full_name}</span>
    <ContributorsList contributors={repository.contributors} />
  </div>
)

export default RepositoryItem
