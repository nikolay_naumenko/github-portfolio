import React, { SFC } from 'react'
import { connect } from 'react-redux'
import { Dispatch } from 'redux'
import { Pagination } from '~/components/pagination'
import { RepositoryItem } from '~/portfolio'
import { fetchRequest } from '~/portfolio/portfolio.actions'
import { ApplicationState } from '~/store'
import { PageData, RepositoriesState, Repository } from './portfolio.types'

import styles from './RepositoryList.scss'

interface RepositoryListDispatchProps {
  getUserRepositories: typeof fetchRequest
}

const DEFAULT_ERROR_MESSAGE = 'User not found. Please check your input.'
const DEFAULT_EMPTY_RESULT_MESSAGE = 'No repositories found.'
const DEFAULT_MESSAGE = 'Please input desired GitHub username.'

const onPaginationClick = (fetch: typeof fetchRequest) => {
  return (e: React.MouseEvent, page: PageData) => {
    e.preventDefault()
    fetch(page.url)
  }
}

const RepositoryList: SFC<RepositoriesState & RepositoryListDispatchProps> = ({
  loading,
  initial,
  data,
  pagination,
  getUserRepositories,
  errors,
}) => (
  <div>
    {initial && <div className={styles.message}>{DEFAULT_MESSAGE}</div>}
    {errors && <div className={styles.error}>{DEFAULT_ERROR_MESSAGE}</div>}
    {!loading &&
      !initial &&
      !errors &&
      data.length === 0 && <div className={styles.error}>{DEFAULT_EMPTY_RESULT_MESSAGE}</div>}
    {!loading &&
      data &&
      data.map((repository: Repository) => (
        <RepositoryItem repository={repository} key={repository.id} />
      ))}
    {!loading &&
      pagination && (
        <Pagination pagination={pagination} onClick={onPaginationClick(getUserRepositories)} />
      )}
    {loading && <div className={styles.message}>Loading...</div>}
  </div>
)

const mapStateToProps = ({ repositories }: ApplicationState) => ({
  initial: repositories.initial,
  loading: repositories.loading,
  data: repositories.data,
  requestUrl: repositories.requestUrl,
  errors: repositories.errors,
  pagination: repositories.pagination,
})

const mapDispatchToProps = (dispatch: Dispatch) => ({
  getUserRepositories: (requestUrl: string) => dispatch(fetchRequest(requestUrl)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RepositoryList)
