import githubUsernameRegex from 'github-username-regex'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Dispatch } from 'redux'
import { Button, Input } from '~/components'
import { fetchRequest } from '~/portfolio/portfolio.actions'
import { getUserReposUrl } from '~/services'
import { ApplicationState } from '~/store'

import styles from './SearchBar.scss'

export interface SearchBarState {
  userName: string
  validationError: boolean
}

interface SearchBarStateProps {
  loading: boolean
}

interface SearchBarDispatchProps {
  getUserRepositories: typeof fetchRequest
}

class SearchBar extends Component<SearchBarStateProps & SearchBarDispatchProps, SearchBarState> {
  state = {
    userName: '',
    validationError: false,
  }

  validate = (userName: string) => !githubUsernameRegex.test(userName)

  onUserNameChangeHandler = (e: React.FormEvent<HTMLInputElement>) => {
    const { value: userName } = e.currentTarget

    this.setState({
      userName,
      validationError: this.validate(userName),
    })
  }

  onSearchButtonClickHandler = (e: React.MouseEvent<HTMLButtonElement>) => {
    e.preventDefault()
    this.props.getUserRepositories(getUserReposUrl(this.state.userName))
  }

  render() {
    const { userName, validationError } = this.state
    const { loading } = this.props

    return (
      <form className={styles.searchBar}>
        <Input
          type="text"
          label="Username: "
          placeholder="GitHub Username"
          value={userName}
          isError={validationError}
          onChange={this.onUserNameChangeHandler}
        />
        <Button
          type="submit"
          onClick={this.onSearchButtonClickHandler}
          disabled={loading || !userName || validationError}
        >
          Retrieve
        </Button>
      </form>
    )
  }
}

const mapStateToProps = ({ repositories }: ApplicationState) => ({
  loading: repositories.loading,
})

const mapDispatchToProps = (dispatch: Dispatch) => ({
  getUserRepositories: (requestUrl: string) => dispatch(fetchRequest(requestUrl)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchBar)
