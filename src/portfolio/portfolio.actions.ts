import { action } from 'typesafe-actions'
import { RepositoriesActionTypes, Repository } from './portfolio.types'

export const fetchRequest = (requestUrl: string) =>
  action(RepositoriesActionTypes.FETCH_REQUEST, requestUrl)

export const fetchSuccess = (data: Repository[]) =>
  action(RepositoriesActionTypes.FETCH_SUCCESS, data)

export const fetchError = (message: string) => action(RepositoriesActionTypes.FETCH_ERROR, message)
