import { Reducer } from 'redux'
import { RepositoriesActionTypes, RepositoriesState } from './portfolio.types'

const initialState: RepositoriesState = {
  initial: true,
  data: [],
  errors: undefined,
  loading: false,
  requestUrl: '',
}

const reducer: Reducer<RepositoriesState> = (state = initialState, action) => {
  switch (action.type) {
    case RepositoriesActionTypes.FETCH_REQUEST: {
      return { ...state, initial: false, loading: true, requestUrl: action.payload, errors: null }
    }
    case RepositoriesActionTypes.FETCH_SUCCESS: {
      return {
        ...state,
        loading: false,
        data: action.payload.data,
        pagination: action.payload.pagination,
        errors: null,
      }
    }
    case RepositoriesActionTypes.FETCH_ERROR: {
      return { ...state, loading: false, errors: action.payload, data: [], pagination: {} }
    }
    default: {
      return state
    }
  }
}

export { reducer as repositoriesReducer }
