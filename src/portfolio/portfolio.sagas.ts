import { effects } from 'redux-saga'
import { getUserRepositories } from '~/services/gitHubApiService'
import { fetchError, fetchRequest, fetchSuccess } from './portfolio.actions'
import { RepositoriesActionTypes } from './portfolio.types'

const { all, call, fork, put, takeEvery } = effects

function* handleFetch(action: ReturnType<typeof fetchRequest>) {
  try {
    const res = yield call(getUserRepositories, action.payload)

    if (res.error) {
      yield put(fetchError(res.error))
    } else {
      yield put(fetchSuccess(res))
    }
  } catch (err) {
    if (err instanceof Error) {
      yield put(fetchError(err.stack!))
    } else {
      yield put(fetchError('An unknown error occurred'))
    }
  }
}

function* watchFetchRequest() {
  yield takeEvery(RepositoriesActionTypes.FETCH_REQUEST, handleFetch)
}

function* repositoriesSaga() {
  yield all([fork(watchFetchRequest)])
}

export default repositoriesSaga
