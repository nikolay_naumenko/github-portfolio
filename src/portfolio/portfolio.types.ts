export type ApiResponse = Record<string, any>

export interface Repository extends ApiResponse {
  id: number
  name: string
  full_name: string
  contributors: Contributor[]
}

export interface Contributor {
  id: number
  avatar_url: string
  login: string
  contributions: number
}

export type PageKeys = 'first' | 'prev' | 'next' | 'last'

export interface PageData {
  url: string
  page: string
  rel: PageKeys
}

export interface Pagination {
  first?: PageData
  prev?: PageData
  next?: PageData
  last?: PageData
}

export const enum RepositoriesActionTypes {
  FETCH_REQUEST = '@@repositories/FETCH_REQUEST',
  FETCH_SUCCESS = '@@repositories/FETCH_SUCCESS',
  FETCH_ERROR = '@@repositories/FETCH_ERROR',
}

export interface RepositoriesState {
  readonly initial: boolean
  readonly loading: boolean
  readonly data: Repository[]
  readonly requestUrl: string
  readonly errors?: string
  readonly pagination?: Pagination
}
