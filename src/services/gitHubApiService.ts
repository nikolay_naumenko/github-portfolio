import axios from 'axios'
import parseLinkHeader from 'parse-link-header'

export const getUserReposUrl = userName =>
  `${process.env.GITHUB_API_URL}users/${userName}/repos?per_page=${
    process.env.GITHUB_ITEMS_PER_PAGE
  }`

export const getContributorsUrl = url => `${url}?per_page=100`

const gitHubApi = axios.create({
  headers: {
    Authorization: `token ${process.env.GITHUB_ACCESS_TOKEN}`,
  },
})

export function getUserRepositories(requestUrl: string) {
  return gitHubApi.get(requestUrl).then(async result => {
    const pagination = parseLinkHeader(result.headers.link)

    await Promise.all(
      result.data.map(repo =>
        gitHubApi
          .get(getContributorsUrl(repo.contributors_url))
          .then(
            res =>
              res &&
              res.data &&
              (repo.contributors = res.data
                .slice()
                .sort((a, b) => b.contributions - a.contributions))
          )
      )
    )

    return {
      data: result.data,
      pagination: pagination,
    }
  })
}
