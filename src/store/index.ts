import { combineReducers, Action, AnyAction, Dispatch } from 'redux'
import { effects } from 'redux-saga'

import { repositoriesReducer } from '~/portfolio/portfolio.reducers'
import repositoriesSaga from '~/portfolio/portfolio.sagas'
import { RepositoriesState } from '~/portfolio/portfolio.types'

const { all, fork } = effects

export interface ApplicationState {
  repositories: RepositoriesState
}

export interface ConnectedReduxProps<A extends Action = AnyAction> {
  dispatch: Dispatch<A>
}

export const rootReducer = combineReducers<ApplicationState>({
  repositories: repositoriesReducer,
})

export function* rootSaga() {
  yield all([fork(repositoriesSaga)])
}
